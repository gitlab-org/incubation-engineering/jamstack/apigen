---
to: .api/package.json
---
{
  "name": "workers-graphql-server",
  "private": true,
  "description": "🔥Lightning-fast, globally distributed Apollo GraphQL server, deployed at the edge using Cloudflare Workers",
  "version": "1.0.1",
  "contributors": [
    "Kristian Freeman <kristian@bytesized.xyz>",
    "Janis Altherr <jaltherr@gitlab.com>"
  ],
  "dependencies": {
    "axios": "^0.27.2",
    "apollo-server": "^3.8.2",
    "apollo-server-cloudflare": "^3.8.1",
    "better-sqlite3": "^7.5.3",
    "browserfs": "^1.4.3",
    "d1-dialect": "^0.0.0-alpha.1",
    "datasource-sql": "^2.0.1",
    "encoding": "^0.1.13",
    "graphql": "^16.5.0",
    "knex": "^2.1.0",
    "path": "^0.12.7",
    "sql.js": "^1.7.0",
    "sqlite3": "^5.0.8",
    "tedious": "^14.5.0",
    "util": "^0.12.4"
  },
  "devDependencies": {
    "prettier": "^2.6.2",
    "webpack": "^5.73.0",
    "webpack-cli": "^4.9.2",
    "node-polyfill-webpack-plugin": "^1.1.4",
    "webpack-graphql-loader": "^1.0.2"
  },
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "format": "prettier --write '**/*.{js,css,json,md}'",
    "dev": "wrangler2 dev --local"
  },
  "license": "MIT"
}