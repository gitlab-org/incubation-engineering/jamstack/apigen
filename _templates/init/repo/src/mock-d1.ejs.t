---
to: .api/src/mock-d1.js
---
const BASE_URL = 'http://localhost:3000'

async function api(method, data) {
  const init = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data),
  };

  const url = `${BASE_URL}/${method}`

  const responseObject = await fetch(url, init)
  const resData = await responseObject.json()

  if (resData.error) {
    throw resData.error
  }
  return resData.result
}

global.env = {
  DB: {
    get(query, params) {
      return api('get', { query, params } )
    },
    all(query, params) {
      return api('all', { query, params } )
    },
    run(query, params) {
      return api('run', { query, params } )
    }
  }
}