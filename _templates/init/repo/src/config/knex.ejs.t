---
to: .api/src/config/knex.js
---
import d1Dialect from 'd1-dialect';

export default {
  client: d1Dialect,
  driverName: 'd1',

  /*
  * Instruct knex to use NULL as a default value when none is passed.
  * This is a required setting when using Sqlite (which D1 uses under the hood)
  */
  useNullAsDefault: true,

  /*
  * Knex requires the connection object to be passed. Fortunately,
  * there is nothing to configure when using d1, so we can pass an
  * empty object instead.
  */
  connection: {}
}