---
to: .api/src/handlers/apollo.js
---
import { ApolloServer, gql } from 'apollo-server-cloudflare';
import {
  graphqlCloudflare,
} from 'apollo-server-cloudflare/dist/cloudflareApollo';
import KVCache from '../kv-cache';
import dataSources from '../datasources';
import resolvers from '../resolvers';
import schema from '../schema.graphql';

const kvCache = { cache: new KVCache() }

const createServer = (graphQLOptions) => {
  return  new ApolloServer({
    typeDefs: gql`${schema}`,
    resolvers,
    introspection: true,
    dataSources,
    ...(graphQLOptions.kvCache ? kvCache : {}),
  })
}

export default async (request, graphQLOptions) => {
  const server = createServer(graphQLOptions)
  await server.start()
  return graphqlCloudflare(() => server.createGraphQLServerOptions(request))(
    request,
  )
}

