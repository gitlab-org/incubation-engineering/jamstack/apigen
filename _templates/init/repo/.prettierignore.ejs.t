---
to: .api/.prettierignore
---
# Ignore artifacts:
dist
node_modules
worker
package*
