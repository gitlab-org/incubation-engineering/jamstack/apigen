---
to: .api/wrangler.toml
---
name = "workers-graphql-server"

# account_id = ""
# zone_id = ""
# route = ""

workers_dev = true
compatibility_date = "2022-05-25"
main = "src/index.js"
# main = "dist/main.js"
node_compat = true

[[rules]]
  type = "Text"
  globs = ["**/*.md", "**/*.graphql"]
  fallthrough = true

# Enable a kv-namespace to use the KV caching feature
#
# kv-namespaces = [
#   { binding = "WORKERS_GRAPHQL_CACHE", id = "" }
# ]

# [build]
#   command = "npx vite dev"
#   watch_dir = "dist"

#
# [build.upload]
# format = "modules"
# main = "./src/index.js"

[dev]
ip = "0.0.0.0"
port = 8787
local_protocol="http"
upstream_protocol="https"
