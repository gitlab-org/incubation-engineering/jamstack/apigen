---
to: .api/.prettierrc
---
{
  "singleQuote": true,
  "semi": false,
  "trailingComma": "all",
  "tabWidth": 2,
  "printWidth": 80
}
