---
inject: true
to: .api/src/schema.graphql
append: true
---
type <%= className %> {
    id: ID!
    name: String!
}

input Create<%= className %>Input {
    name: String!
}

input Update<%= className %>Input {
    id: ID!
    name: String
}

input Delete<%= className %>Input {
    id: ID!
}