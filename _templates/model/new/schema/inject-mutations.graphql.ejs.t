---
inject: true
to: .api/src/schema.graphql
after:  !!js/regexp /type Mutation \{/g
---
    create<%= className %>(input: Create<%= className %>Input!): <%= className %>
    update<%= className %>(input: Update<%= className %>Input!): <%= className %>
    delete<%= className %>(input: Delete<%= className %>Input!): Boolean!