---
inject: true
to: .api/src/schema.graphql
after:  !!js/regexp /type Query \{/g
---

    <%= attributeName %>(id: ID!): <%= className %>
    <%= attributeName %>s: [<%= className %>]