---
to: .api/src/datasources/<%= fileName %>.js
---
import { SQLDataSource } from "datasource-sql";

export default class <%= className %> extends SQLDataSource {
  async get(id) {
    return this.knex.select('*').from('<%= tableName %>').where({
    id })
  }
  async all(id) {
    return this.knex.select('*').from('<%= tableName %>')
  }
  async create(data) {
    return this.knex.insert(data).into('<%= tableName %>')
  }
  async update(id, data) {
    throw "Method not implemented"
  }
  async delete(id) {
    throw "Method not implemented"
  }
}
