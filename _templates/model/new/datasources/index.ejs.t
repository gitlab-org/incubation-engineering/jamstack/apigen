---
inject: true
to: .api/src/datasources/index.js
before: !!js/regexp /\}\)$/g
---
  <%= attributeName %>: new <%= className %>(knexConfig),