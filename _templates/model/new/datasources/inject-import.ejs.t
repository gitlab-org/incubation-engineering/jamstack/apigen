---
inject: true
to: .api/src/datasources/index.js
after: !!js/regexp /^import .* from ['"`].*['"`];?$/g
---
import <%= className %> from './<%= fileName %>';