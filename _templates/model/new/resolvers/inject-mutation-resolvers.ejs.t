---
inject: true
to: .api/src/resolvers/index.js
after: !!js/regexp /Mutation. \{/
---
    ...<%= className %>Resolvers.mutations,