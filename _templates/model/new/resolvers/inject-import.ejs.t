---
inject: true
to: .api/src/resolvers/index.js
before: !!js/regexp /(^import .* from ['"`].*['"`];?$)*/g
---
import <%= className %>Resolvers from './<%= fileName %>';