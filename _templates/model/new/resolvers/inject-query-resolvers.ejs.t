---
inject: true
to: .api/src/resolvers/index.js
after: !!js/regexp /Query[:] \{/g
---
    ...<%= className %>Resolvers.queries,