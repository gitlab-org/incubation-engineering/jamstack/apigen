---
to: .api/src/resolvers/<%= fileName %>.js
---
export default {
  queries: {
    async <%= attributeName %>(_source, { id }, { dataSources }) {
      try {
        return await dataSources.<%= attributeName %>.get(id)
      } catch (error) {
        return { error }
      }
    },
    async <%= attributeName %>s() {
      try {
        return await dataSources.<%= attributeName %>.all(id)
      } catch (error) {
        return { error }
      }
    }
  },
  mutations: {
    async create<%= className %>(_, input, { dataSources }) {
      try {
        return await dataSources.<%= attributeName %>.create(input)
      } catch (error) {
        return { error }
      }
    },
    update<%= className %>(input) {},
    delete<%= className %>(input) {}
  }
}