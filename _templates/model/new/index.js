module.exports = {
  params: ({ args, h }) => {
    return {
      tableName: h.changeCase.snake(args.name),
      className: h.changeCase.pascal(args.name),
      attributeName: h.changeCase.camel(args.name),
      fileName: h.changeCase.param(args.name)
    }
  }
}